const { ApolloServer, gql } = require('apollo-server');

// This is a (sample) collection of books we'll be able to query
// the GraphQL server for.  A more complete example might fetch
// from an existing data source like a REST API or database.
const { RESTDataSource } = require('apollo-datasource-rest');

class LivredorAPI extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = 'http://localhost:4001/';
  }

  async getUsers() {
  	return this.get(`users`);	
  }

  async getUser(id) {
  	return this.get(`users/user/${id}`);
  }

  async getUserByUsername(username) {
    return this.get(`users/search/${username}`);
  }

  async createUser(user) {
  	return this.post(`users/create`, user);
  }

  async createMessage(message) {
    return this.post(`messages/create`, message);
  }

  async removeMessage(message) {
    return this.post(`messages/remove`, message);
  }
}

// Type definitions define the "shape" of your data and specify
// which ways the data can be fetched from the GraphQL server.
const typeDefs = gql`
  # Comments in GraphQL are defined with the hash (#) symbol.

  type Message {
  	id: ID!
    content: String
  }

  type User {
  	_id: ID!
  	username: String
  	messages: [Message]
  }

  # The "Query" type is the root of all GraphQL queries.
  type Query {
    users: [User]
    user(id: ID, username: String): User
  }

  type Mutation {
  	createMessage(userId: String, content: String) : Message
  	createUser(username: String) : User
    removeMessage(userId: String!, messageId: String!) : User
  }
`;

// Resolvers define the technique for fetching the types in the
// schema.  We'll retrieve books from the "books" array above.
const resolvers = {
	Query: {
    	users: async (_source, {  }, { dataSources }) => {
      		return dataSources.livredorAPI.getUsers();
    	},
    	user: async (_source, { id, username }, { dataSources }) => {
      		if(id){
            return dataSources.livredorAPI.getUser(id);
          }
          if(username){
            return dataSources.livredorAPI.getUserByUsername(username);
          }
    	},
  	},
  	Mutation: {
  		createMessage: async (_source, { userId, content }, {dataSources}) => {
  			return dataSources.livredorAPI.createMessage({ userId : userId, content : content});
  		},

  		createUser: async (_source, { username }, {dataSources}) => {
  			return dataSources.livredorAPI.createUser({ username : username });
  		},

      removeMessage: async (_source, {userId, messageId}, {dataSources}) => {
        return dataSources.livredorAPI.removeMessage({ userId : userId, messageId : messageId});
      }
  	}
};

// In the most basic sense, the ApolloServer can be started
// by passing type definitions (typeDefs) and the resolvers
// responsible for fetching the data for those types.
const server = new ApolloServer({ 
	typeDefs, 
	resolvers,
	dataSources: () => {
    return {
      	livredorAPI: new LivredorAPI(),
    	};
  	},
  	context: () => {
    	return {
      		token: 'foo',
    	};
  	},
});

// This `listen` method launches a web-server.  Existing apps
// can utilize middleware options, which we'll discuss later.
server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});