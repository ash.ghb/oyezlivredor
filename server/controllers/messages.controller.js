var config = require('config.json');
var express = require('express');
var router = express.Router();
var messageService = require('services/message.service');

// routes
router.post('/create', create);
router.post('/remove', remove);

module.exports = router;

function create(req, res) {
    messageService.create(req.body)
        .then(function (user) {
            res.send(user.messages.pop());
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function remove(req, res) {
    messageService.remove(req.body)
        .then(function (user) {
            res.send(user);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}