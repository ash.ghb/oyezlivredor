require('rootpath')();
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var Promise = require('promise');
var config = require('config.json');

//var mongo = require('mongodb').MongoClient;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// routes
app.use('/users', require('./controllers/users.controller'));
app.use('/messages', require('./controllers/messages.controller'));
// start server
var port = process.env.NODE_ENV === 'production' ? 80 : 4001;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});