// TODO LOTS OF REFACTO (mongo connect/settimeout etc...)
var config = require('config.json');
var _ = require('lodash');
var Promise = require('promise');

var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectId;

var service = {}

service.insertOne = insertOne;
service.all = all;
service.find = find;
service.first = first;
service.where = where;
service.searchOrCreate = searchOrCreate;
service.replace = replace;

module.exports = service;

/**
  * Insere un document dans la collection
  */
function insertOne(collectionName, item){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);

	   		setTimeout(function(db, collectionName, item){
	   			db.collection(collectionName).insertOne(item,
	        	function (err, doc) {
	            	if (err) reject(err.name + ': ' + err.message);
            		
	            	resolve(doc.ops[0]);
	    		});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName, item);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	})
    return promise;
}

/**
  * Retourne tous les elements d'une collection
  */
function all(collectionName){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);
	   		
	   		setTimeout(function(db, collectionName){
	   			db.collection(collectionName).find({})
	   				.toArray(function (err, items) {
	            		if (err) reject(err.name + ': ' + err.message);
		            	resolve(items);
	    			});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	})
    return promise;	
}

/**
  * Retrouve l'element d'une collection avec l'id correspondant
  */
function find(collectionName, id, notFoundCallBack = null){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);
	   		setTimeout(function(db, collectionName, id, notFoundCallBack){
	   			find_object = {_id : ObjectId(id)};
	   			db.collection(collectionName).findOne(find_object, function (err, item) {
	            		if (err) reject(err.name + ': ' + err.message);
		            	
		            	if(item){
		            		resolve(item);
		            	}
		            	else{ // item not found
		            		if(notFoundCallBack != null)
		            			notFoundCallBack();
		            		else
		            			resolve();
		            	}
	    			});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName, id, notFoundCallBack);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	});
    return promise;	
}

/**
  * Retourne le premier element correspondant au critère précisé (field == value) [field ne peut pas être un id]
  */
function first(collectionName, field, value, notFoundCallBack = null){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);
	   		setTimeout(function(db, collectionName, field, value, notFoundCallBack){
	   			find_object = {};
	   			find_object[field] = value;
	   			db.collection(collectionName).findOne(find_object, function (err, item) {
	            		if (err) reject(err.name + ': ' + err.message);
		            	
		            	if(item){
		            		resolve(item);
		            	}
		            	else{ // item not found
		            		if(notFoundCallBack != null)
		            			notFoundCallBack();
		            		else
		            			resolve();
		            	}
	    			});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName, field, value, notFoundCallBack);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	})
    return promise;	
}

/**
  * Retourne tous les elements correspondants au critère précisé (field == value) [field ne peut pas être un id]
  */
function where(collectionName, field, value, notFoundCallBack = null){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);
	   		
	   		setTimeout(function(db, collectionName, field, value, notFoundCallBack){
	   			find_object = {};
	   			find_object[field] = value;
	   			db.collection(collectionName).find(find_object).toArray(function (err, items) {
	            		if (err) reject(err.name + ': ' + err.message);
		            	
		            	if(items){
		            		resolve(items);
		            	}
		            	else{ // item not found
		            		if(notFoundCallBack != null)
		            			notFoundCallBack();
		            		else
		            			resolve();
		            	}
	    			});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName, field, value, notFoundCallBack);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	})
    return promise;	
}

/**
  * Retourne le premier element correspondant au critère précisé (field == value) [field ne peut pas être un id] 
  * Si aucun document n'est trouvé, en crée un nouveau avec l'attribut "messages" (mis à une liste vide)
  */
function searchOrCreate(collectionName, field, value){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);
	   		
	   		setTimeout(function(db, collectionName, field, value){
	   			find_object = {};
	   			find_object[field] = value;
	   			insert_object = {};
	   			insert_object[field] = value;
   				insert_object.messages = [];
	   			db.collection(collectionName).findAndModify(find_object, [['_id','asc']], insert_object, {new: true, upsert: true}, function (err, item) {
	            		if (err) reject(err.name + ': ' + err.message);
		            	
		            	resolve(item.value);
	    			});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName, field, value);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	})
    return promise;	
}

/**
  * Trouve un document et le replace en retournant le nouveau element.
  */
function replace(collectionName, id, doc){
	var promise = new Promise(function(resolve, reject){
		mongo.connect(config.connectionString, { useNewUrlParser: true })
		.then(function(client, err){
			var db = client.db(config.connectionDbName);
	   		
	   		setTimeout(function(db, collectionName, id, doc){
	   			find_object = {_id : ObjectId(id)};
	   			db.collection(collectionName).findOneAndReplace(find_object, doc, {returnOriginal : false}, function (err, item) {
	            		if (err) reject(err.name + ': ' + err.message);
		            	
		            	resolve(item.value);
	    			});
	   			client.close();
	   		}, 1000+Math.floor(Math.random() * 1000), db, collectionName, id, doc);
   		})
   		.catch(function(err){
   			console.log(err);
   		});
	})
    return promise;	
}

