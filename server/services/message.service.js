var config = require('config.json');

var mongoD = require('utils/mongo-driver');
var ObjectId = require('mongodb').ObjectId;
var userSerivce = require('services/user.service');
var service = {};

service.create = create;
service.remove = remove;

module.exports = service;

function create(message) {
	return mongoD.find('Users', message.userId)
		.then(function(user){
			var doc = {id: new ObjectId(), content : message.content};
			user.messages.push(doc)
			return mongoD.replace('Users', message.userId, user);
		});
}

function remove(args) {
	return mongoD.find('Users', args.userId)
		.then(function(user){
			var element = user.messages.find(function(element){
				return element.id == args.messageId;
			});
			var index = user.messages.indexOf(element);
			if (index > -1){
				user.messages.splice(index, 1);
			}
			return mongoD.replace('Users', args.userId, user);
		});
}