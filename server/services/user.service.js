var config = require('config.json');
var _ = require('lodash');
var Promise = require('promise');
var mongoD = require('utils/mongo-driver');
var service = {};

service.create = create;
service.getAll = getAll;
service.find = find;
service.search = search;


module.exports = service;

function create(user) { // todo username should be unique
    return mongoD.insertOne('Users', user);
}

function getAll() {
	return mongoD.all('Users');
}

function find(_userId) {
	return mongoD.find('Users', _userId);
}

function search(_username) {
	return mongoD.first('Users', 'username', _username)
		.then(function(user){
			if(!user){
				return mongoD.insertOne('Users', {username : _username, messages : []});
			}
			else
				return user;
		})
}

