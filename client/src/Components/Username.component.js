import React, { Component } from 'react';
import gql from 'graphql-tag';

const GET_USER = gql`
  query get_user($username : String!){
    user(username: $username){
      _id
      username
      messages{
        id
        content
      }
    }
  }
`;

class Username extends Component {
  constructor(props) {
    super(props);
    this.state = {username: null, loading: false};
  }

  handleChange(e) {
    this.setState({ username: e.target.value });
  }

  gotUser(user){
    this.setState({ loading : false });
    this.props.callbackFromParent(user);
  }

  selectUsername(){
    if(this.state.username !== ""){
      this.setState({ loading : true });
      let query_variables = {username : this.state.username}
      this.props.client.query({
        query: GET_USER,
        variables: query_variables
      }).then(({data}) => this.gotUser(data.user)).catch(err => console.log(err));
    }
    else
      alert("No selected Username");
  }

  render() {
    return (
      <div className="username">
        <input disabled={this.state.loading} placeholder="Nom d'utilisateur" onChange={(e) => this.handleChange(e)}/>
        <button onClick={(e) => this.selectUsername(e)} >Valider</button>
        {this.state.loading == true &&
          <p>Chargement des données en cours..</p>
        }
      </div>
    );
  }
}

export default Username;
