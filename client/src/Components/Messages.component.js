import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

/**
  * GQL QUERIES & MUTATIONS
  */

const REMOVE_MESSAGE = gql`
  mutation remove_message($userId : String!, $messageId : String!){
    removeMessage(userId: $userId, messageId: $messageId){
      _id
      username
      messages{
        id
        content
      }
    }
  }
`;

const POST_MESSAGE = gql`
  mutation post_message($userId : String!, $content : String!){
    createMessage(userId: $userId, content: $content){
      id
      content
    }
  }
`;

const GET_USER = gql`
  query get_user($username : String!){
    user(username: $username){
      _id
      username
      messages{
        id
        content
      }
    }
  }
`;

/**
  * Classe
  */

class Messages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messageContent : "",
      inputBlocked: false
    }
  }
  // Pour mettre à jour suivant le contenu de l'input
  handleChange(e) {
    this.setState({ messageContent: e.target.value });
  }

  // Pour l'ajout de message
  handleKeyPress(e){
    if(e.key === 'Enter'){
      if(this.state.messageContent !== ""){
        let post_variables = {userId : this.props.user._id, content: this.state.messageContent}
        this.setState({inputBlocked : true});
        this.props.client.mutate({
          mutation: POST_MESSAGE,
          variables: post_variables,
          // optimistic responses
          optimisticResponse: {
            __typename: "Mutation",
              createMessage: {
                __typename: "Message",
                content: this.state.messageContent
              }
          },
          // MàJ du cache
          update: (proxy, { data: {createMessage}}) => {
              // Read the data from our cache for this query.
              const data = proxy.readQuery({ query: GET_USER, variables: {username : this.props.user.username} });
              // Add our comment from the mutation to the end.
              data.user.messages.push(createMessage);
              // Write our data back to the cache.
              proxy.writeQuery({ query: GET_USER, data });
              this.props.callbackFromParent(data.user);
              this.setState({messageContent : ''});
              this.setState({inputBlocked : false});
          },
          // refetching queries
          refetchQueries: [{
            query: GET_USER,
            variables: {username : this.props.user.username}
          }]
        },).then(({data}) => console.log("Added to db", data)).catch(err => console.log(err));
      } else {
        alert("Vous ne pouvez pas ajouter un message vide..");
      }
    }
  }

  // Pour la suppression de messages
  handleClick(e){
    // New User pour le passer à la réponse optimiste
    var newUser = this.props.user;
    var element = newUser.messages.find(function(element){
        return element.id == e.target.id;
      });
    var index = newUser.messages.indexOf(element);
    if (index > -1){
      newUser.messages.splice(index, 1);
    }

    // Variables to make the mutation
    let remove_variables = {userId : this.props.user._id, messageId: e.target.id}
    
    this.props.client.mutate({
      mutation: REMOVE_MESSAGE,
      variables: remove_variables,
      // Ma réponse optimise
      optimisticResponse: {
        __typename: "Mutation",
          removeMessage: {
            __typename: "User",
            messages: newUser.messages
          }
      },
      // MàJ du cache
      update: (proxy, { data: {removeMessage}}) => {
          // Read the data from our cache for this query.
          const data = proxy.readQuery({ query: GET_USER, variables: {username : this.props.user.username} });
          // Add our comment from the mutation to the end.
          data.user.messages = removeMessage.messages;
          // Write our data back to the cache.
          proxy.writeQuery({ query: GET_USER, data });
          this.props.callbackFromParent(data.user);
      },
      // refetching queries
      refetchQueries: [{
        query: GET_USER,
        variables: {username : this.props.user.username}
      }]
    },).then(({data}) => console.log("Removed from db", data)).catch(err => console.log(err));
  }

  render() {
    return (
      <table>
        <tbody>
          <tr>
            <td>
              <input placeholder="Insérez un nouveau message" disabled={this.state.inputBlocked} value={this.state.messageContent} onChange={(e) => this.handleChange(e)} onKeyPress={(e) => this.handleKeyPress(e)} />
            </td>
          </tr>
          {this.props.user.messages.map(( message, key ) => {
          return (
            <tr key={message.id ? message.id : key} onClick={(e) => this.handleClick(e)}>
              <td id={message.id ? message.id : key}>
                {message.content}
              </td>
            </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
}

export default Messages;
