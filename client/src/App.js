import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Username from './Components/Username.component';
import Messages from './Components/Messages.component';


class App extends Component {
  constructor(props) {
        super(props);
        this.state = {
            user: null
        };
  }

  getUser = (user) => {
    this.setState({user : user});
  }

  postMessage = (user) => {
    this.setState({user : user}); 
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Bienvenue au livre d'or Oyez !</h1>
        </header>
        <Username client={this.props.client} callbackFromParent={this.getUser}/>
        {this.state.user !== null &&
        <Messages client={this.props.client} user={this.state.user} callbackFromParent={this.postMessage}/>
        }
      </div>
    );
  }
}

export default App;
