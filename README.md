# Livre d'Or Oyez

Ce projet est un mini-test du stack : **Mongo**/**Node**/**Express**/**Appolo**/**React**.

# Structure du projet
Le projet se divise en 3 dossiers: 
* **Client** : Contient la partie React
* **Appolo** : Contient la partie Appolo Server
* **Server** : Contient la partie Node ("crud"), elle est essentiellement composée de trois sous dossiers:
	* *controllers*
	*  *services*
	*  *utils* : il contient un fichier (*mongo-driver.js*) qui est en fait une réecriture des fonctions MongoDB (la librairie native pour Node) avec la simulation de lenteur (*1~2s*) : En bref, contient toute la logique Mongo.

# Installation
Les étapes de l'installation de l'application sont :
1. Lancer une base de donnée MongoDB, créer une db et une collection `Users`,
2. Se rendre dans le dossier *server*, modifier le fichier config.json avec les informations relatives à votre base de donnée et ensuite, taper (dans le dossier server, toujours) les lignes de commandes `npm install`, puis `node server.js`,
3. Se rendre dans le dossier *appolo*, taper les lignes de commandes `npm install`, puis `node index.js`,
4. Se rendre dans le dossier *client*, taper les lignes de commandes `npm install`, puis `npm start`,
5. Si la page ne s'ouvre pas d'elle même, se rendre sur [http://localhost:3000](http://localhost:3000).


